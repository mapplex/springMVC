

Announcements:
--------------
- 整合adminLTE 的 spring mvc 后端管理平台
Special Features:
-----------------
- **Fully responsive**
- **Enhanced for printing**
- **Sortable dashboard widgets**
- **18 plugins and 3 custom plugins**
- **Light weight and fast**
- **Compatible with most major browsers**
- **Full support for Glyphicons, Fontawesome and Ion icons**

Featured Pages:
----------------
- Dashboard
- Mailbox
- Calendar
- Invoice
- Lockscreen
- Login
- Register
- 404 Error
- 500 Error
- Blank page

Featured Plugins:
-----------------
- Boostrap Slider
- Ion slider
- Bootstrap WYSIHTML5
- CKEditor
- Bootstrap Colorpicker
- Bootstrap Date range Picker
- Bootstrap Time Picker
- Data Tables
- Flot
- Morris.js
- Sparkilne
- Full Calendar
- iCheck
- jQuery input mask
- jQuery Knob
- jVector Map
- Slim Scroll
- Pace
- [Bootstrap Social Buttons](http://lipis.github.io/bootstrap-social/ "Bootstrap Social")

支持浏览器:
----------------
- IE 9+
- Firefox 5+
- Chrome 14+
- Safari 5+
- Opera 11+


