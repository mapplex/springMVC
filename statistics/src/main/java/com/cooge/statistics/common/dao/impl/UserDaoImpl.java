package com.cooge.statistics.common.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cooge.statistics.common.dao.UserDao;
import com.cooge.statistics.common.model.User;
import com.cooge.statistics.common.util.BaseDao;
import com.cooge.statistics.common.util.SQLUtil;
@Repository
public class UserDaoImpl extends BaseDao<User> implements UserDao {

	@Override
	public List<User> findByList(String[] params, int first, int max) {
		List<User> list =this.query("SELECT "+SQLUtil.getParams(params)+" FROM main_user limit "+first+","+max);
		return list;
	}

	@Override
	public List<User> findByList(String[] params) {
		return null;
	}

	@Override
	public int totalNum(String[] params) {
		return 0;
	}

	@Override
	public Class<User> getEntityClass() {
		return User.class;
	}

	@Override
	public User checkUser(String name, String password) {
		
		List<User> 	ulist = this.query("select * from main_user where name = ? and password = ?", new String[]{name,password});
		if(ulist.size()>0){
		return	ulist.get(0);
		}
		return null;
	}

	@Override
	public User getById(Integer id) {
		List<User> 	ulist = this.query("select * from main_user where id = ?", new Integer[]{id});
		if(ulist.size()>0){
			return ulist.get(0);
		}
		return null;
	}


}
