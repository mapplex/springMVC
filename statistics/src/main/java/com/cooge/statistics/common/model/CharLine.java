package com.cooge.statistics.common.model;

public class CharLine implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5225203704639446899L;
	private String id;
	private String name;
	private Long number;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}
	
}
