package com.cooge.statistics.common.service;

import com.cooge.statistics.common.model.UserInfo;

public interface UserInfoService {
	
	UserInfo findByUserId(Integer id);
	
	boolean updateSystemset(Integer id,String systemset);
}
