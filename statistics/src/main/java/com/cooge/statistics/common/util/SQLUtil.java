package com.cooge.statistics.common.util;

public class SQLUtil {
	
	public static String getParams(String[] params){
		String str = "";
		for(String p:params){
			if(!p.trim().equals(""))
			str = ","+p+str;
		}
		if(str.length()>0){
			str = str.substring(1);
		}else{
			str = "*";
		}
		return str;
	}

}
