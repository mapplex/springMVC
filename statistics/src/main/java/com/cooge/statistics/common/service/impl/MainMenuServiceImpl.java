package com.cooge.statistics.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooge.statistics.common.dao.MainMenuDao;
import com.cooge.statistics.common.model.MainMenu;
import com.cooge.statistics.common.service.MainMenuService;
import com.cooge.statistics.common.util.MenuUtil;
@Service
public class MainMenuServiceImpl implements MainMenuService {

	@Autowired
	MainMenuDao mainMenuDao;
	
	@Override
	public List<MainMenu> findByList(String[] params) {
		List<MainMenu> mlist = mainMenuDao.findByList(params);
		mlist.addAll(MenuUtil.getSystemMenu());
		return mlist;
	}

	@Override
	public List<MainMenu> findByNOSMList(String[] params) {
		List<MainMenu> mlist = mainMenuDao.findByList(params);
		return mlist;
	}

}
