package com.cooge.statistics.common.service;

import java.util.List;

import com.cooge.statistics.common.model.MainMenu;

public interface MainMenuService {
	
	List<MainMenu> findByList(String [] params);
	
	List<MainMenu> findByNOSMList(String [] params);
}
