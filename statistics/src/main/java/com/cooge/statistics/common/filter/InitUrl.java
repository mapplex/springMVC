package com.cooge.statistics.common.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.cooge.statistics.common.util.URLUtil;

public class InitUrl implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest request = ((HttpServletRequest)arg0);
		
		if(request.getAttribute(URLUtil.WEBURL)==null&&(!request.getServletPath().contains("/json/"))&&(!request.getServletPath().contains("/chart/"))){
			
			request.setAttribute(URLUtil.WEBURL, URLUtil.getBasePath(request));
		}
		arg2.doFilter(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
