package com.cooge.statistics.common.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cooge.statistics.common.service.UserService;
import com.cooge.statistics.common.util.BaseController;
import com.cooge.statistics.common.util.PageInfo;

@Controller
@RequestMapping("user")
public class UserController extends BaseController{
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public String userlist(HttpServletRequest request,HttpServletResponse response) throws IOException{
		return "user/list";
	}
	
//	@RequestMapping(value = "json/list", method = RequestMethod.POST,produces = "text/html;charset=UTF-8")
//	@ResponseBody
//	public String list(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo) throws IOException{
//		return objectToJson(userService.findDataTable(pageInfo));
//	}
	
	@RequestMapping(value = "list", method = RequestMethod.POST)
	@ResponseBody
	public Object listjson(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo) throws IOException{
		
		return userService.findDataTable(pageInfo);
	}

}
