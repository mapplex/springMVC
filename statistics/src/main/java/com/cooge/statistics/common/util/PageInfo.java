package com.cooge.statistics.common.util;

public class PageInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4978788532383158257L;
	private int sEcho;
	private int iColumns;
	private String[] sColumns;
	private int iDisplayStart;
	private int iDisplayLength;
	private String sSearch;

	public int getsEcho() {
		return sEcho;
	}

	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}

	public int getiColumns() {
		return iColumns;
	}

	public void setiColumns(int iColumns) {
		this.iColumns = iColumns;
	}

	public String[] getsColumns() {
		return sColumns;
	}

	public void setsColumns(String[] sColumns) {
		this.sColumns = sColumns;
	}

	public int getiDisplayStart() {
		return iDisplayStart;
	}

	public void setiDisplayStart(int iDisplayStart) {
		this.iDisplayStart = iDisplayStart;
	}

	public int getiDisplayLength() {
		return iDisplayLength;
	}

	public void setiDisplayLength(int iDisplayLength) {
		this.iDisplayLength = iDisplayLength;
	}

	public String getsSearch() {
		return sSearch;
	}

	public void setsSearch(String sSearch) {
		this.sSearch = sSearch;
	}

}
