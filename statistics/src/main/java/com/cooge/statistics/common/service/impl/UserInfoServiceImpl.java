package com.cooge.statistics.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooge.statistics.common.dao.UserInfoDao;
import com.cooge.statistics.common.model.UserInfo;
import com.cooge.statistics.common.service.UserInfoService;
@Service
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	UserInfoDao userInfoDao;
	
	@Override
	public UserInfo findByUserId(Integer id) {
		return userInfoDao.findByUserId(new String[]{"u.id","u.name","userId","u.systemset","updateTime"}, id);
	}

	@Override
	public boolean updateSystemset(Integer id, String systemset) {
		return userInfoDao.updateSystemset(id, systemset);
	}

}
