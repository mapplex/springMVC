package com.cooge.statistics.common.model;


/**
 * MainMenu entity. @author MyEclipse Persistence Tools
 */
public class MainMenu implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 8798192953746710889L;
	private Integer id;
	private String title;
	private String url;
	private Integer pid;
	private Integer sequence;
	private String icon;
	// Constructors

	/** default constructor */
	public MainMenu() {
	}

	/** full constructor */
	public MainMenu(String title, String url, Integer pid, Integer sequence) {
		this.title = title;
		this.url = url;
		this.pid = pid;
		this.sequence = sequence;
	}

	// Property accessors
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getPid() {
		return this.pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getSequence() {
		return this.sequence;
	}

	public void setSequence(Integer sequence) {
		this.sequence = sequence;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

}