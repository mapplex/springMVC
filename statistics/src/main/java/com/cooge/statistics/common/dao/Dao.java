package com.cooge.statistics.common.dao;

import java.util.List;

public interface Dao<T> {
   
	List<T> findByList(String [] params,int first,int max);
	
	List<T> findByList(String [] params);
	
	int totalNum(String [] params);
	
}
