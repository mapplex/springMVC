package com.cooge.statistics.common.util;

import java.util.List;

public class DataTablesJson<T> {
	
	
	private int sEcho = 1;

	private String iTotalRecords;

	private String iTotalDisplayRecords;

	private List<T> aaData;

	public int getsEcho() {
		return sEcho;
	}

	public void setsEcho(int sEcho) {
		this.sEcho = sEcho;
	}

	
	public DataTablesJson(PageInfo pageinfo,Integer totalNum,List<T> hlist){
		
		this.setAaData(hlist);
		this.setsEcho(pageinfo.getsEcho()+1);
		this.setiTotalRecords(String.valueOf(totalNum));
		this.setiTotalDisplayRecords(String.valueOf(totalNum));
		
	}
	
	public String getiTotalRecords() {
		return iTotalRecords;
	}

	public void setiTotalRecords(String iTotalRecords) {
		this.iTotalRecords = iTotalRecords;
	}

	public String getiTotalDisplayRecords() {
		return iTotalDisplayRecords;
	}

	public void setiTotalDisplayRecords(String iTotalDisplayRecords) {
		this.iTotalDisplayRecords = iTotalDisplayRecords;
	}

	public List<T> getAaData() {
		return aaData;
	}

	public void setAaData(List<T> aaData) {
		this.aaData = aaData;
	}

}
