package com.cooge.statistics.common.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cooge.statistics.common.dao.UserInfoDao;
import com.cooge.statistics.common.model.UserInfo;
import com.cooge.statistics.common.util.BaseDao;
import com.cooge.statistics.common.util.SQLUtil;
@Repository
public class UserInfoDaoImpl extends BaseDao<UserInfo> implements UserInfoDao {

	@Override
	public List<UserInfo> findByList(String[] params, int first, int max) {
		
		return null;
	}

	@Override
	public List<UserInfo> findByList(String[] params) {
	//	List<UserInfo> list =this.query("SELECT "+SQLUtil.getParams(params)+" FROM main_user m  left join user_info u on u.userId  = m.id where u.id =?",new Integer[]{});
		return null;
	}

	@Override
	public int totalNum(String[] params) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Class<UserInfo> getEntityClass() {
		return UserInfo.class;
	}

	@Override
	public UserInfo findByUserId(String[] params,Integer id) {
		List<UserInfo> list =this.query("SELECT "+SQLUtil.getParams(params)+" FROM main_user m  left join user_info u on u.userId  = m.id where u.id =?",new Integer[]{id});
		if(list.size()>0){
			return list.get(0);
		}
		return null;
	}

	@Override
	public boolean updateSystemset(Integer id, String systemset) {
		int i = this.getJdbcTemplate().update("update user_info set systemset = ? where id = ?",new Object[]{systemset,id});
		if(i>0){
			return true;
		}
		return false;
	}

}
