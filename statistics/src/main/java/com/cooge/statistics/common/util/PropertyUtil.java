package com.cooge.statistics.common.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

/**
 * ��ȡ�����ļ�
 * 
 * @author Administrator
 * 
 */
public class PropertyUtil extends PropertyPlaceholderConfigurer {

	private static Map<String, Object> ctxPropertiesMap;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactoryToProcess,Properties props) throws BeansException {
		super.processProperties(beanFactoryToProcess, props);
		ctxPropertiesMap = new HashMap<String, Object>();
		for (Object key : props.keySet()) {
			String keyStr = key.toString();
			String value = props.getProperty(keyStr);
			ctxPropertiesMap.put(keyStr, value);
			String[] keys = keyStr.split("\\.");
			if(keys.length==2){
				Object o  = ctxPropertiesMap.get(keys[0]);
				if(o==null){
					o= new HashMap<String, Object>();
					ctxPropertiesMap.put(keys[0], o);
				}
				((Map)o).put(keys[1], value);
				
			}
		}
	}
	public static Object getContextProperty(String name) {
		return ctxPropertiesMap.get(name);
	}
}