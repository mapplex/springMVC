package com.cooge.statistics.common.util;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

public class URLUtil {
	
	public static final String UID = "USER";
	
	public static final String REQUESTURL = "REQUESTURL";
	
	public static final String REQUESTTITLE = "REQUESTTITLE";
	
	public static final String LEFTMENU = "LEFTMENU";
	
	public static final String REQUESNAV = "REQUESNAV";
	
	public static final String WEBURL ="WebUrl";
	
	public static final String SYSTEMSET = "SYSTEMSET";
	
	public static final String SUCCESS = "success";
	
	public static final String ERROR = "error";
	
	public static final String THEME = "theme";
	
	public static final String PURL = "pUrl";
	
	public static String getBasePath(HttpServletRequest request) throws IOException{
		
		String weburl = request.getContextPath();
		request.setCharacterEncoding("utf-8");
		String Scheme = request.getScheme();//http
		String localAddr =  request.getServerName();//127.0.0.1
		int port = request.getLocalPort();//8080
		if(port==80){
			weburl = Scheme+"://"+localAddr+weburl;
		}else{
			weburl = Scheme+"://"+localAddr+":"+port+weburl;
		}
		return weburl+"/";
	}
	
	public static String getServletPath(HttpServletRequest request) throws IOException{
		String url = request.getServletPath().substring(1);
        while(url.startsWith("/")){
        	url = url.substring(1);
        }
		return url;
		
	}

}
