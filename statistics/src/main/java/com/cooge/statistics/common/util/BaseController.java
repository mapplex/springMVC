package com.cooge.statistics.common.util;

import java.util.List;

import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.cooge.statistics.common.model.BarChart;
import com.cooge.statistics.common.model.BarChartValue;
import com.cooge.statistics.common.model.CharLine;
import com.cooge.statistics.common.model.ChartValue;
import com.cooge.statistics.common.model.DonutChart;

public class BaseController {


	public ModelAndView linecChatView(ModelMap map, List<CharLine> charlineList) {
		map.addAttribute("charlinedata", JSON.toJSONString(charlineList));
		ModelAndView mav = new ModelAndView("component/line-chart");
		return mav;
	}

	public ModelAndView donutChartView(ModelMap map, DonutChart donutChart) {
		map.addAttribute("colors", JSON.toJSONString(TypeUtil.getColos(donutChart.getChartValues().size())));
		map.addAttribute("chartValues",JSON.toJSONString(donutChart.getChartValues()));
		ModelAndView mav = new ModelAndView("component/donut-chart");
		return mav;
	}

	public ModelAndView barChartView(ModelMap map, BarChart barChart) {
		List<BarChartValue> chartValuesList = barChart.getBarChartValues();
		String as = "";
		String labes = "";
		int i = 0;
		for (BarChartValue barChartValue : chartValuesList) {
			String cs = "x:" + "'" + barChartValue.getName() + "'";
			for (ChartValue cv : barChartValue.getChartValues()) {
				cs = cs + "," + cv.getLabel() + ":" + cv.getValue();
				if (i == 0) {
					labes = labes + ",'" + cv.getLabel() + "'";
				}
			}
			cs = "{" + cs + "}";
			as = as + "," + cs;
			i++;
		}
		labes = "[" + labes.substring(1) + "]";
		as = "[" + as.substring(1) + "]";
		ModelAndView mav = new ModelAndView("component/bar-chart");
		mav.addObject("data", as);
		mav.addObject("labes", labes);
		return mav;

	}
}
