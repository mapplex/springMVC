package com.cooge.statistics.common.dao;

import com.cooge.statistics.common.model.User;

public interface UserDao extends Dao<User>{
	
	User checkUser(String name,String password);
	
	
	User getById(Integer id);

}
