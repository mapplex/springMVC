package com.cooge.statistics.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cooge.statistics.common.model.MainMenu;

public class MenuUtil {
	
	private static List<MainMenu> menulist = new ArrayList<MainMenu>();
	/**
	 * 自主添加动态菜单,建议 ID > 65536 防止冲突
	 * @param main
	 */
	
	public static void  add(MainMenu main){
		menulist.add(main);
	}
	
	public static Map<Integer, List<MainMenu>> findByMap(List<MainMenu> mlist) {
		Map<Integer, List<MainMenu>> map = new HashMap<Integer, List<MainMenu>>();
	    for(MainMenu m:mlist){
	    	List<MainMenu> list = map.get(m.getPid());
	    	if(list==null){
	    		map.put(m.getPid(), new ArrayList<MainMenu>());
	    		list = map.get(m.getPid());
	    	}
	    	list.add(m);
	    }
		return map;
	}
	
	public static List<MainMenu> getSystemMenu(){
		List<MainMenu> mlist = new ArrayList<MainMenu>();
		////////////////////////
		MainMenu menu= new MainMenu();
		menu.setIcon("<i class=\"fa fa-cog\"></i>");
		menu.setTitle("系统设置");
		menu.setId(65536);
		menu.setPid(0);
		menu.setSequence(0);
		menu.setUrl("#");
		mlist.add(menu);
		////////////////////////////////////////
		MainMenu menu1= new MainMenu();
		menu1.setIcon("<i class=\"fa fa-user\"></i>");
		menu1.setTitle("用户管理");
		menu1.setId(65535);
		menu1.setPid(65536);
		menu1.setSequence(0);
		menu1.setUrl("#");
		mlist.add(menu1);
		
		MainMenu menu3= new MainMenu();
		menu3.setIcon("<i class=\"fa fa-user\"></i>");
		menu3.setTitle("用户管理");
		menu3.setId(65533);
		menu3.setPid(65536);
		menu3.setSequence(0);
		menu3.setUrl("#");
		mlist.add(menu3);
		
		////////////////////////////////////////
		MainMenu menu2= new MainMenu();
		menu2.setIcon("<i class=\"fa fa-list-ul\"></i>");
		menu2.setTitle("用户列表");
		menu2.setId(65534);
		menu2.setPid(65535);
		menu2.setSequence(0);
		menu2.setUrl("user/list.html");
		mlist.add(menu2);
		
		////////////////////////////////////////
		MainMenu menu4= new MainMenu();
		menu4.setIcon("<i class=\"fa fa-list-ul\"></i>");
		menu4.setTitle("用户列表");
		menu4.setId(65532);
		menu4.setPid(65533);
		menu4.setSequence(0);
		menu4.setUrl("#");
		mlist.add(menu4);
		////////////////////////
		MainMenu menu5= new MainMenu();
		menu5.setIcon("<i class=\"fa fa-cog\"></i>");
		menu5.setTitle("菜单管理");
		menu5.setId(65531);
		menu5.setPid(65536);
		menu5.setSequence(1);
		menu5.setUrl("system/menuManager.html");
		mlist.add(menu5);
		mlist.addAll(menulist);
		return mlist;
	}
	

}
