package com.cooge.statistics.common.model;

import java.util.List;

public class BarChartValue implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7779796755889278276L;
	
	private String name;
	
	private List<ChartValue>  chartValues;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ChartValue> getChartValues() {
		return chartValues;
	}

	public void setChartValues(List<ChartValue> chartValues) {
		this.chartValues = chartValues;
	}



	
	
	
	
	

}
