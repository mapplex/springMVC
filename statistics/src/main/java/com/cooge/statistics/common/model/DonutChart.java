package com.cooge.statistics.common.model;

import java.util.List;

public class DonutChart implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8206666592900542015L;

	private String[] colors;
	
	private List<ChartValue> chartValues;

	public String[] getColors() {
		return colors;
	}

	public void setColors(String[] colors) {
		this.colors = colors;
	}

	public List<ChartValue> getChartValues() {
		return chartValues;
	}

	public void setChartValues(List<ChartValue> chartValues) {
		this.chartValues = chartValues;
	}
}
