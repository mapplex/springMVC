package com.cooge.statistics.common.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import com.cooge.statistics.common.model.MainMenu;
import com.cooge.statistics.common.service.MainMenuService;
import com.cooge.statistics.common.util.LoadClassUtil;
import com.cooge.statistics.common.util.MenuUtil;
import com.cooge.statistics.common.util.UIUtil;
import com.cooge.statistics.common.util.URLUtil;

public class PageFilter implements Filter {

	
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1,FilterChain arg2) throws IOException, ServletException {
		HttpServletRequest request = ((HttpServletRequest) arg0);
		System.out.println(request.getRequestURI());
		
		if((!request.getServletPath().contains("/json/"))&&(!request.getServletPath().contains("/chart/"))&&(!request.getServletPath().contains("/table/"))){
	        request.setAttribute(URLUtil.REQUESTURL, URLUtil.getServletPath(request));
			request.setAttribute(URLUtil.REQUESTTITLE,"��ҳ");
			request.setAttribute(URLUtil.REQUESNAV, "<li class=\"active\">��ҳ</li>");

			MainMenuService mainMenuService = new LoadClassUtil<MainMenuService>().getBeanClass(request, MainMenuService.class);
			List<MainMenu> menulist = mainMenuService.findByList(new String[] {});
			String leftmenu = UIUtil.initMenu(MenuUtil.findByMap(menulist),0,request.getAttribute(URLUtil.WEBURL).toString(), request);
			request.setAttribute(URLUtil.LEFTMENU, leftmenu);
		}
		arg2.doFilter(arg0, arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
