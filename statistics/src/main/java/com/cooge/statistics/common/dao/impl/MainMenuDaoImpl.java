package com.cooge.statistics.common.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cooge.statistics.common.dao.MainMenuDao;
import com.cooge.statistics.common.model.MainMenu;
import com.cooge.statistics.common.util.BaseDao;
import com.cooge.statistics.common.util.SQLUtil;
@Repository
public class MainMenuDaoImpl extends BaseDao<MainMenu> implements MainMenuDao {

	@Override
	public Class<MainMenu> getEntityClass() {
		return MainMenu.class;
	}

	@Override
	public List<MainMenu> findByList(String[] params, int first, int max) {
		List<MainMenu> list =this.query("SELECT "+SQLUtil.getParams(params)+" FROM main_menu  limit ?,?", new Object[]{first,max});
		return list;
	}

	@Override
	public List<MainMenu> findByList(String[] params) {
		List<MainMenu> list =this.query("SELECT "+SQLUtil.getParams(params)+" FROM main_menu order by sequence asc");
		return list;
	}

	@Override
	public int totalNum(String[] params) {
		return this.queryTotalNum("select count(*) FROM main_menu");
	}

	
}
