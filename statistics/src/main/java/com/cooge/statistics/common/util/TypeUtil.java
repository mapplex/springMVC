package com.cooge.statistics.common.util;

import java.util.ArrayList;
import java.util.List;

public class TypeUtil {

	public static final String HMTB = "hmtb";
	public static final String HDTB = "hdtb";
	public static final String hwtb = "hwtb";

	private static List<String> colosList = new ArrayList<String>();

	public static String TypeToTableName(String type) {
		String str = null;

		if (type.equals(HMTB)) {
			str = "hot_month_top_before";
		}
		if (type.equals(HDTB)) {
			str = "hot_day_top_before";
		}
		if (type.equals(hwtb)) {
			str = "hot_week_top_before";
		}

		return str;
	}

	public static String[] getColos(int i) {
		if (colosList.size() == 0) {
			colosList.add("#0000CC");
			colosList.add("#003300");
			colosList.add("#663333");
			colosList.add("#3333FF");
			colosList.add("#00FF66");
			colosList.add("#33CC66");
			colosList.add("#33FF33");
			colosList.add("#333366");
			colosList.add("#6666CC");
			colosList.add("#660066");
			colosList.add("#339900");
			colosList.add("#669999");
			colosList.add("#00CC66");
			colosList.add("#66CC99");
			colosList.add("#666633");
			colosList.add("#33CC00");
			colosList.add("#330033");
			colosList.add("#009999");
			colosList.add("#0066FF");
			colosList.add("#006600");
		}
		return colosList.subList(0, i).toArray(new String[] {});
	}

}
