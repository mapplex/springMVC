package com.cooge.statistics.common.dao;

import com.cooge.statistics.common.model.UserInfo;

public interface UserInfoDao extends Dao<UserInfo>{
	
	UserInfo findByUserId(String[] params,Integer id);
	
	boolean updateSystemset(Integer id,String systemset);

}
