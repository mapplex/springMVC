package com.cooge.statistics.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooge.statistics.common.dao.UserDao;
import com.cooge.statistics.common.model.User;
import com.cooge.statistics.common.service.UserService;
import com.cooge.statistics.common.util.DataTablesJson;
import com.cooge.statistics.common.util.PageInfo;
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserDao userDao;
	
	@Override
	public User checkUser(String name, String password) {
		return userDao.checkUser(name, password);
	}


	@Override
	public DataTablesJson<User> findDataTable(PageInfo pageinfo) {
		List<User> hlist = userDao.findByList(pageinfo.getsColumns(), pageinfo.getiDisplayStart(), pageinfo.getiDisplayLength());
		int totalnum = userDao.totalNum(new String[]{pageinfo.getsSearch()});
		DataTablesJson<User> htable = new DataTablesJson<User>(pageinfo,totalnum,hlist);
		return htable;
	}

	@Override
	public User getById(Integer id) {
		return userDao.getById(id);
	}

}
