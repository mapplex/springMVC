package com.cooge.statistics.common.service;

import com.cooge.statistics.common.model.User;
import com.cooge.statistics.common.util.DataTablesJson;
import com.cooge.statistics.common.util.PageInfo;


public interface UserService {
	
	User checkUser(String name,String password);
	
	DataTablesJson<User> findDataTable(PageInfo pageinfo);
	
	User getById(Integer id);

}
