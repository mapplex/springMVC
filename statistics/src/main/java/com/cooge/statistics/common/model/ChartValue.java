package com.cooge.statistics.common.model;

public class ChartValue implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2967133352843235733L;

	private String label;
	
	private Long value;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Long getValue() {
		return value;
	}

	public void setValue(Long value) {
		this.value = value;
	}
	
	

}
