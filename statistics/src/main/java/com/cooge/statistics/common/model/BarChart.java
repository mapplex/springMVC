package com.cooge.statistics.common.model;

import java.util.List;

public class BarChart implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4309209635363367542L;


	private List<BarChartValue> barChartValues;


	public List<BarChartValue> getBarChartValues() {
		return barChartValues;
	}


	public void setBarChartValues(List<BarChartValue> barChartValues) {
		this.barChartValues = barChartValues;
	}



}
