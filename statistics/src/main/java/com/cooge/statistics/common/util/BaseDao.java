package com.cooge.statistics.common.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class BaseDao<T> {
    
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public abstract Class<T> getEntityClass(); 
	

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	public List<T> query(String sql,Object[] objects){
		List<T> list = jdbcTemplate.query(sql,objects,new BeanPropertyRowMapper<T>(this.getEntityClass()));
		return list;
	}
	
	public List<T> query(String sql){
		List<T> list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<T>(this.getEntityClass()));
		return list;
	}
	
	public int queryTotalNum(String sql){
		return jdbcTemplate.queryForObject(sql, Integer.class);
	}
}
