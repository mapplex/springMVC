package com.cooge.statistics.common.util;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class LoadClassUtil<T> {
	
	
	public T getBeanClass(HttpServletRequest request,Class<T> cls){
		ServletContext context =  request.getSession().getServletContext();
		ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);
		return ctx.getBean(cls);
	}

}
