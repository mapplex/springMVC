package com.cooge.statistics.common.model;

import java.util.Date;

public class UserInfo implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4203799024892880114L;
	
	private Integer id;
	
	private Integer userId;
	
	private String name;
	
	private String systemset;
	
	private Date updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSystemset() {
		return systemset;
	}

	public void setSystemset(String systemset) {
		this.systemset = systemset;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
	
	

}
