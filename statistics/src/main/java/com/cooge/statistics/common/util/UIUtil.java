package com.cooge.statistics.common.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.cooge.statistics.common.model.MainMenu;
//class=\"active\"
public class UIUtil {

	public static String initMenu(Map<Integer, List<MainMenu>> map, Integer id,String url,HttpServletRequest request) {
		List<MainMenu> mlist = map.get(id);
		if(mlist==null){
			return "";
		}
		String str = "";
		for (int i = 0; i < mlist.size(); i++) {
			MainMenu menu = mlist.get(i);
			String hclass = "";
			String showi="";
			String showCSS = "";
			String icon = "";
			String active = "";
			if(map.get(menu.getId())!=null&&map.get(menu.getId()).size()>0){
				showi = "<i class=\"fa pull-right fa-angle-left\"></i>";
				hclass  ="class=\"treeview\"";
				
			}else{
				String murl = request.getAttribute(URLUtil.REQUESTURL).toString();
				if(murl.equals(menu.getUrl())){
					active = "class=\"active\"";
					initTile(menu,map,request);
				}
			}
			if((menu.getIcon()==null||menu.getIcon().trim().equals(""))){
				if(!id.equals(0)){
					showCSS = "style=\"margin-left: 10px;\"";
					icon  ="<i class=\"fa fa-angle-double-right\"></i>";
				}else{
					icon  ="<i class=\"fa fa-dashboard\"></i>";
				}
			}else{
				icon = menu.getIcon();
			}
			String _url = url;
			if(menu.getUrl()==null||menu.getUrl().equals("#")){
				_url = "";
			}
			String childrenHtml  = initMenu(map, menu.getId(),url,request);
			if(childrenHtml.contains("class=\"active\"")){
				hclass  ="class=\"treeview  active \"";
			}
			str =str+ "<li "+hclass+active+" ><a href=\"" +_url+ menu.getUrl()+ "\" "+showCSS+" >"+icon+ "<span>"+menu.getTitle()+"</span>"+showi+"</a>";
			str = str+ childrenHtml;
			str = str+ "</li>";
		}
		if (mlist.size() > 0) {
			
			if(id.equals(0)){
			}else{
				str = "<ul class=\"treeview-menu\" style=\"display: none;\"  >" + str + "</ul>";
			}
			
		}
		return str;
	}
	
	public static void initTile(MainMenu menu,Map<Integer, List<MainMenu>> map,HttpServletRequest request){
				Map<Integer, MainMenu> maps = new HashMap<Integer, MainMenu>();
				Iterator<List<MainMenu>> iterator = map.values().iterator();;
				while(iterator.hasNext()){
					List<MainMenu> im = iterator.next();
					for(int i  = 0;i<im.size();i++){
						maps.put(im.get(i).getId(), im.get(i));
					}
				}
				MainMenu menu_1 = menu;
				int  i  = 0;
				String nav = "";
				while(true){
					if(i==0){
						nav = "<li class=\"active\">"+menu_1.getTitle()+"</li>";
					}else{
						nav = "<li><a href=\"#\"><i class=\"fa fa-dashboard\"></i>"+menu_1.getTitle()+"</a></li>";
					}
					i++;
					if(menu_1.getPid().equals(0)){
						break;
					}
					if(!menu_1.getPid().equals(0)){
							menu_1 = maps.get(menu_1.getPid());
					}
				}
				nav = "<li><a href=\"#\"><i class=\"fa fa-dashboard\"></i>��ҳ</a></li>"+nav;
				request.setAttribute(URLUtil.REQUESTTITLE, menu.getTitle());
				request.setAttribute(URLUtil.REQUESNAV, nav);
	}

}
