package com.cooge.statistics.project.model;

public class ReVideo implements java.io.Serializable{

	
	
private static final long serialVersionUID = -4721907767854605469L;

private String	id;

private String title;

private String actor;

private String director;

private String description;

private String region;

private String category;

private String year;

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getTitle() {
	return title;
}

public void setTitle(String title) {
	this.title = title;
}

public String getActor() {
	return actor;
}

public void setActor(String actor) {
	this.actor = actor;
}

public String getDirector() {
	return director;
}

public void setDirector(String director) {
	this.director = director;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public String getRegion() {
	return region;
}

public void setRegion(String region) {
	this.region = region;
}

public String getCategory() {
	return category;
}

public void setCategory(String category) {
	this.category = category;
}

public String getYear() {
	return year;
}

public void setYear(String year) {
	this.year = year;
}



	
}
