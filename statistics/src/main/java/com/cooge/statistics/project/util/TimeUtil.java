package com.cooge.statistics.project.util;

import java.text.ParseException;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class TimeUtil {
	
	public static XMLGregorianCalendar convertToXMLGregorianCalendar(Date date) {

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);
        XMLGregorianCalendar gc = null;
        try {
            gc = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (Exception e) {

             e.printStackTrace();
        }
        return gc;
    }
	
	
	public static String XMLGregorianCalendarToString(XMLGregorianCalendar xml) {
        return xml.toXMLFormat().substring(0, 10);
    }
	
	/**
	 * 通过当前的时间得到一个时间间隔
	 */
	
	public static String getIntervalTime(Date date,int num){
		String startTime = format(date,"yyyy-MM-dd");
		String endTime =  format(new Date(date.getTime()-24*3600000*num),"yyyy-MM-dd");
		return "("+startTime+")-("+endTime+")";
	}
	
	public static String getNowTime(){
		java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String s = format1.format(new Date());
		return s;
	}
	//i为天数
	
	public static String getTime(int i){
		java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String s = null;
		s = format1.format(new Date().getTime()+i*24*3600*1000);
		return s;
	}
	
	public static String getYTime(){
		java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String s = format1.format(new Date().getTime()-24*3600*1000);
		return s;
	}
	public static String getMTime(){
		java.text.DateFormat format1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String s = format1.format(new Date().getTime()+24*3600*1000);
		return s;
	}
	
	public static String format(Date date,String formet){
		java.text.DateFormat format1 = new java.text.SimpleDateFormat(formet);
		String s = format1.format(date);
		return s;
	}
	
	public static Date format(String time,String formet){
		java.text.DateFormat format1 = new java.text.SimpleDateFormat(formet);
		try {
			return format1.parse(time);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
}
