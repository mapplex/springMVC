package com.cooge.statistics.project.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;


public class NetUtil {
	private final  Log logger = LogFactory.getLog(this.getClass());
	
	public void logTest(String url,Exception e){
		logger.info("����:"+url+"-------��ȡʧ��");
		logger.info(e.getMessage());
	}
	
	public synchronized static void DownloadDocument(String url,SyncCallBack syncCallBack){
		
		try {
			Thread.sleep(5000);
			new Thread(new DownHtml(syncCallBack, url)).start();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	public static class DownHtml implements  Runnable 
	{
		 private SyncCallBack syncCallBack;
		 private String url;
		 
		 public DownHtml(SyncCallBack syncCallBack,String url){
			 this.syncCallBack = syncCallBack;
			 this.url = url;
		 }
		@Override
		public void run() {
			String doc = NetUtil.getDocument(url);
			syncCallBack.onSuccess(doc);
		}
		
	}
	
	
	public static abstract class SyncCallBack{
		
		public abstract void onSuccess(String doc);
		
	}
	
	public static String getDocument(String url){
		int i = 0;
		while(true){
			try {
				return NetUtil.getHtml(url);
			} catch (Exception e) {
				new NetUtil().logTest(url, e);
				i++;
				if(i>6){
					return "";
				}
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
		}
		
	}
	public static String getHtml(String url) throws Exception{
		HttpClient	httpclient = HttpClients.createDefault();
		HttpGet httpget = new HttpGet(url);
			HttpResponse response = httpclient.execute(httpget);
			HttpEntity entity = response.getEntity();
			InputStream in = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in,"UTF-8"));
			StringBuilder sb = new StringBuilder();
			String line = null;
			while((line = reader.readLine()) != null){
				sb.append(line + "\r\n"); 
			}
			//System.out.println(sb.toString());
			System.out.println(url);
			return sb.toString();
	}
	
	
	

}
