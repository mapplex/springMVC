package com.cooge.statistics.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cooge.statistics.common.util.BaseController;
@Controller
@RequestMapping("weibo")
public class WeiBoController extends BaseController{
	
	/*
	private MangeService mangeService =  HotData.getMangeServiceFactory();
	
//	@RequestMapping(value = "day", method = RequestMethod.GET)
//	public String day(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit) throws IOException{
//		
//		return objectToJson(mangeService.findDaySinafollowByList(pageInfo, new DaySinafollow(),timeLimit));
//	}
	
//	@RequestMapping(value = "day", method = RequestMethod.GET)
//	public Object day(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit) throws IOException{
//		
//		return mangeService.findDaySinafollowByList(pageInfo, new DaySinafollow(),timeLimit);
//	}
	
	
	
	@RequestMapping(value = "{timeLimit}/type_{styleId}", method = RequestMethod.GET)
	public String daytype(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId) throws IOException{
		map.addAttribute("time", timeLimit);
		return "weibo/day";
	}
	
	@RequestMapping(value = "table/{timeLimit}/type_{styleId}", method = RequestMethod.GET)
	public String dayhot(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId){
		String type =  String.valueOf(PropertyUtil.getContextProperty(StyleCache.SINA+StyleCache.getValue(String.valueOf(styleId)).trim()));
		WeibokeywordData ds  = new WeibokeywordData();
		ds.setVideoType(type);
		ds.setStartTime(ds.getStartTime());
		pageInfo.setIDisplayLength(50);
		
		String view="";
		
		if(timeLimit.equals("week")){
			timeLimit = TimeUtil.getIntervalTime(new Date(),7);
			view = "weibo/table";
		}else{
			timeLimit = null;
			view = "weibo/table";
		}
		
		List<WeibokeywordData> slist  = mangeService.findDayweibofollowByList(pageInfo, ds, timeLimit);
		map.addAttribute("slist", slist);
		return view;
	}
	*/
}
