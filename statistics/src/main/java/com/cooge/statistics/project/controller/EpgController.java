package com.cooge.statistics.project.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSON;
import com.cooge.statistics.common.util.PropertyUtil;
import com.cooge.statistics.project.model.Epg;
import com.cooge.statistics.project.model.Hot;
import com.cooge.statistics.project.model.ReVideo;
import com.cooge.statistics.project.util.NetUtil;

@Controller
@RequestMapping("epg")
public class EpgController {
	
	@RequestMapping(value = "recommend", method = RequestMethod.GET)
	public String index(HttpServletRequest request,HttpServletResponse response) throws IOException{
		return "epg/recommend";
	}
	
	@RequestMapping(value = "table/recommend", method = RequestMethod.GET)
	public String table(ModelMap map,HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		List<Epg> epglist = Epg.getList();
		request.setAttribute("epglist", epglist);
		return "epg/index";
	}
	
	@RequestMapping(value = "table/video", method = RequestMethod.GET)
	public String video(ModelMap map,HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		try {
			String type = PropertyUtil.getContextProperty("sina_"+request.getParameter("type")).toString();
			String videoid = request.getParameter("videoid");
			String str = NetUtil.getHtml("http://61.129.70.51/json/LiveToVod/"+type+videoid+".json");
			List<ReVideo> rlist= JSON.parseArray(str, ReVideo.class);
			map.addAttribute("rlist", rlist);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "epg/table";
	}
	@RequestMapping(value = "currentHot", method = RequestMethod.GET)
	public String currentHot(ModelMap map,HttpServletRequest request,HttpServletResponse response) throws IOException{
		
		
	try {
		String url = "http://api2.kanketv.com/api/v1/epg/currentHotEpg.json?appKey=34DB874AF269B539&appScrect=40&pageNo=1&pageSize=16";
		String json = NetUtil.getHtml(url);
		String jsonarray = JSON.parseObject(json).getJSONObject("kanke").getJSONArray("list").toJSONString();
		List<Hot> hlist = JSON.parseArray(jsonarray, Hot.class);
		map.addAttribute("hlist", hlist);
	} catch (Exception e) {
		e.printStackTrace();
	}
		return "epg/currenthot";
	}

}
