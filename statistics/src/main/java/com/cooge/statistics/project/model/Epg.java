package com.cooge.statistics.project.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.cooge.statistics.common.util.PropertyUtil;
import com.cooge.statistics.project.util.NetUtil;

public class Epg {

	public static String[] alltepg = { "hunan", "jiangsu", "zhejiang","dongfang", "beijing", "shenzhenweishi" };

	private static List<Epg> epglist = new ArrayList<Epg>();

	private static Map<String, List<Jiemu>> epgMap = new HashMap<String, List<Jiemu>>();

	public static List<Jiemu> getJiemus(String name, String time) {
		
		String url = PropertyUtil.getContextProperty("epgurl").toString();
		List<Jiemu> jiemus = Epg.epgMap.get(name + "_" + time);
		if (jiemus == null || jiemus.size() == 0) {
			try {
				String str = NetUtil.getHtml(url.replace("[type]", name).replace("[date]", time));
				String ajson = JSON.parseObject(str).getJSONObject("kanke").getJSONArray("list").toJSONString();
				List<Jiemu> jlist = JSON.parseArray(ajson, Jiemu.class);
				Epg.epgMap.put(name + "_"+ time, jlist);
				return jlist;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return jiemus;
	}

	public static List<Epg> getList() {

		if (epglist.size() == 0) {
			for (String style : Epg.alltepg) {
				Epg mEpg = new Epg();
				mEpg.setStyle(style);
				mEpg.setName(PropertyUtil.getContextProperty(style).toString());
				epglist.add(mEpg);

			}
		}
		return epglist;
	}

	private String style;

	private String name;

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
