package com.cooge.statistics.project.util;

import java.util.HashMap;
import java.util.Map;

public class StyleCache {
	
	public static String SINA = "sina_";
	
	private static Map<String,String> hashMap = new HashMap<String,String>();
	
	
	public static String getValue(String key){
		return String.valueOf(hashMap.get(key));
	}
	
	public static String putValue(String key,String value){
		return hashMap.put(key, value);
	}

}
