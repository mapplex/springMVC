package com.cooge.statistics.project.model;

import java.util.List;

public class Hot implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2025912717302008950L;

	private String title;
	
	private String director;
	
	private String actor;
	
	private String classId;
	
	private String imageLink;
	
	private String lpic;
	
	private String videoId;
	
	private String desc;
	
	private String bpic;
	
	private List<Hepg> epgs;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getActor() {
		return actor;
	}

	public void setActor(String actor) {
		this.actor = actor;
	}

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getImageLink() {
		return imageLink;
	}

	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}

	public String getLpic() {
		return lpic;
	}

	public void setLpic(String lpic) {
		this.lpic = lpic;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getBpic() {
		return bpic;
	}

	public void setBpic(String bpic) {
		this.bpic = bpic;
	}

	public List<Hepg> getEpgs() {
		return epgs;
	}

	public void setEpgs(List<Hepg> epgs) {
		this.epgs = epgs;
	}

}
