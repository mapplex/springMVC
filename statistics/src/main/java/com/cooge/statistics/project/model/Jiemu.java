package com.cooge.statistics.project.model;

public class Jiemu  implements java.io.Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8314719776745163034L;
	private String startTime;
	private String	endTime;
	private String percentage;
	private String intervalTime;
	private String englishName;
	private String chaneseName;
	private String imageLink;
	private String lpic;
	private String bpic;
	private String mpic;
	private String title;
	private String classId;
	private String videoId;
	private String director;
	private String actor;
	private String desc;
	private String oriTitle;
	private Boolean now ;
	private Boolean next;
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getIntervalTime() {
		return intervalTime;
	}
	public void setIntervalTime(String intervalTime) {
		this.intervalTime = intervalTime;
	}
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	public String getChaneseName() {
		return chaneseName;
	}
	public void setChaneseName(String chaneseName) {
		this.chaneseName = chaneseName;
	}
	public String getImageLink() {
		return imageLink;
	}
	public void setImageLink(String imageLink) {
		this.imageLink = imageLink;
	}
	public String getLpic() {
		return lpic;
	}
	public void setLpic(String lpic) {
		this.lpic = lpic;
	}
	public String getBpic() {
		return bpic;
	}
	public void setBpic(String bpic) {
		this.bpic = bpic;
	}
	public String getMpic() {
		return mpic;
	}
	public void setMpic(String mpic) {
		this.mpic = mpic;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getClassId() {
		return classId;
	}
	public void setClassId(String classId) {
		this.classId = classId;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getActor() {
		return actor;
	}
	public void setActor(String actor) {
		this.actor = actor;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getOriTitle() {
		return oriTitle;
	}
	public void setOriTitle(String oriTitle) {
		this.oriTitle = oriTitle;
	}
	public Boolean getNow() {
		return now;
	}
	public void setNow(Boolean now) {
		this.now = now;
	}
	public Boolean getNext() {
		return next;
	}
	public void setNext(Boolean next) {
		this.next = next;
	}
	

}
