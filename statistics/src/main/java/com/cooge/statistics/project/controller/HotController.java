package com.cooge.statistics.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cooge.statistics.common.util.BaseController;

@Controller
@RequestMapping("hot")
public class HotController extends BaseController{
	/*
	
	private MangeService mangeService =  HotData.getMangeServiceFactory();
	
	@RequestMapping(value = "index", method = RequestMethod.GET)
	public String index(HttpServletRequest request,HttpServletResponse response) throws IOException{
		return "hot/index";
	}
//	@RequestMapping(value = "json/day", method = RequestMethod.POST,produces = "text/html;charset=UTF-8")
//	@ResponseBody
//	public String jsonday(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo) throws IOException{
//		
//		return objectToJson(mangeService.findRankHotByTableJson(pageInfo, new RankHot()));
//	}
	
	
	@RequestMapping(value = "day", method = RequestMethod.POST)
	public Object json_day(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo) throws IOException{
		return mangeService.findRankHotByTableJson(pageInfo, new RankHot());
	}
	
	@RequestMapping(value = "day", method = RequestMethod.GET)
	public String day(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map) throws IOException{
		return "hot/day";
	}
	
	
	@RequestMapping(value = "{timeLimit}/type_{styleId}", method = RequestMethod.GET)
	public String daytype(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId) throws IOException{
		List<RankWebsite> rkslist = mangeService.findRankWebsiteByList(new ArrayList<String>());
		map.addAttribute("time", timeLimit);
		map.addAttribute("rkslist", rkslist);
		map.addAttribute("styleId", styleId);
		return "hot/day";
	}
	@RequestMapping(value = "table/{timeLimit}/type_{styleId}/webSite_{webSiteId}", method = RequestMethod.GET)
	public String dayhot(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId,@PathVariable Integer webSiteId){
		String view="";
		RankHot rankHot = new RankHot(); 
		rankHot.setAddTime(mangeService.getLastTime());
		rankHot.setStyleId(styleId);
		rankHot.setWebsiteId(webSiteId);
		pageInfo.setIDisplayLength(1000);
		map.addAttribute("time", timeLimit);
		if(timeLimit.equals("week")){
			timeLimit = TimeUtil.getIntervalTime(new Date(),7);
			view = "hot/timetable";
		}else{
			timeLimit = null;
			view = "hot/table";
		}
		List<RankHot> hotlist = mangeService.findRankHotByList(pageInfo,rankHot,timeLimit);
		map.addAttribute("hotlist", hotlist);
		return view;
	}
	
	@RequestMapping(value = "table/{timeLimit}/type_{styleId}", method = RequestMethod.GET)
	public String dayAllhot(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId){
		String view="";
		RankHot rankHot = new RankHot();
		rankHot.setAddTime(mangeService.getLastTime());
		rankHot.setStyleId(styleId);
		pageInfo.setIDisplayLength(50);
		map.addAttribute("time", timeLimit);
		if(timeLimit.equals("week")){
			timeLimit = TimeUtil.getIntervalTime(new Date(),7);
			view = "hot/alltimetable";
		}else{
			timeLimit = null;
			view = "hot/alltable";
		}
		List<RankHot> hotlist = mangeService.findRankHotByALLTOP(pageInfo,rankHot,timeLimit);
		map.addAttribute("hotlist", hotlist);
		map.addAttribute("styleId", styleId);
		return view;
	}
	@RequestMapping(value = "chart/{timeLimit}/type_{styleId}/videoId_{videoId}", method = RequestMethod.GET)
	public ModelAndView dayhotChat(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId,@PathVariable Integer videoId){
		RankHot rankHot = new RankHot(); 
		rankHot.setAddTime(mangeService.getLastTime());
		rankHot.setStyleId(styleId);
		rankHot.setVideoId(videoId);
		pageInfo.setIDisplayLength(1000);
		map.addAttribute("time", timeLimit);
		if(timeLimit.equals("week")){
			timeLimit = TimeUtil.getIntervalTime(new Date(),7);
		}else{
			timeLimit = null;
		}
		List<RankHot> hotlist = mangeService.findRankHotByChatList(pageInfo,rankHot,timeLimit);
		List<ChartValue> cvlist =new ArrayList<ChartValue>();
		for(RankHot rh:hotlist){
			if(rh.getCount()!=null&&rh.getCount()>0){
				ChartValue cv  = new ChartValue();
				cv.setLabel(rh.getWebsitename());
				cv.setValue(rh.getCount());
				cvlist.add(cv);
			}
		}
		DonutChart donutChart = new DonutChart();
		donutChart.setChartValues(cvlist);
		
		return donutChartView(map, donutChart);
	}
	@RequestMapping(value = "chart/line/{timeLimit}/type_{styleId}/videoId_{videoId}", method = RequestMethod.GET)
	public ModelAndView dayhotlineChat(HttpServletRequest request,HttpServletResponse response,PageInfo pageInfo,ModelMap map,@PathVariable String timeLimit,@PathVariable Integer styleId,@PathVariable Integer videoId){
		RankHot rankHot = new RankHot(); 
		rankHot.setVideoId(videoId);
		rankHot.setAddTime(mangeService.getLastTime());
		pageInfo.setIDisplayLength(1000);
		map.addAttribute("time", timeLimit);
		List<CharLine> charlineList = new ArrayList<CharLine>();
		if(timeLimit.equals("week")){
			timeLimit = TimeUtil.getIntervalTime(new Date(),7);
		}else{
			timeLimit = null;
		}
		List<RankHot> hotlist = mangeService.findRankHotByLineChatList(pageInfo,rankHot,timeLimit);
		for(RankHot rh:hotlist){
			CharLine mCharLine=new CharLine();
			mCharLine.setNumber(rh.getCount());
			mCharLine.setName(TimeUtil.XMLGregorianCalendarToString(rh.getAddTime()));
			charlineList.add(mCharLine);
		}
		return linecChatView(map,charlineList);
	}
	*/

}
