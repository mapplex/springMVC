$(function() {
    /* For demo purposes */
    var demo = $("<div />").css({
        position: "fixed",
        top: "150px",
        right: "0",
        background: "rgba(0, 0, 0, 0.7)",
        "border-radius": "5px 0px 0px 5px",
        padding: "10px 15px",
        "font-size": "16px",
        "z-index": "999999",
        cursor: "pointer",
        color: "#ddd"
    }).html("<i class='fa fa-gear'></i>").addClass("no-print");

    var demo_settings = $("<div />").css({
        "padding": "10px",
        position: "fixed",
        top: "130px",
        right: "-200px",
        background: "#fff",
        border: "3px solid rgba(0, 0, 0, 0.7)",
        "width": "200px",
        "z-index": "999999"
    }).addClass("no-print");
    demo_settings.append(
            "<h4 style='margin: 0 0 5px 0; border-bottom: 1px dashed #ddd; padding-bottom: 3px;'>\u9762\u677f\u8bbe\u7f6e</h4>"
            + "<div class='form-group no-margin'>"
            + "<div class='.checkbox'>"
            + "<label>"
            + "<input type='checkbox' onchange='change_layout();'/> "
            + "Fixed layout"
            + "</label>"
            + "</div>"
            + "</div>"
            );
   var  demo_settingshtml = "<h4 style='margin: 0 0 5px 0; border-bottom: 1px dashed #ddd; padding-bottom: 3px;'>Skins</h4>"
    + "<div class='form-group no-margin'>"
    + "<div class='.radio'>"
    + "<label>";
   if($("body").hasClass("skin-black")){
	   demo_settingshtml = demo_settingshtml + "<input name='skins' type='radio' onchange='change_skin(\"skin-black\");'  checked='checked' /> ";
   }else{
	   demo_settingshtml = demo_settingshtml + "<input name='skins' type='radio' onchange='change_skin(\"skin-black\");' /> ";
   }
   demo_settingshtml = demo_settingshtml  + "Black"
    + "</label>"
    + "</div>"
    + "</div>"
    + "<div class='form-group no-margin'>"
    + "<div class='.radio'>"
    + "<label>";
   if($("body").hasClass("skin-blue")){
	   demo_settingshtml = demo_settingshtml + "<input name='skins' type='radio' onchange='change_skin(\"skin-blue\");' checked='checked'/> ";
   }else{
	   demo_settingshtml = demo_settingshtml + "<input name='skins' type='radio' onchange='change_skin(\"skin-blue\");' /> ";
   }
   demo_settingshtml = demo_settingshtml+ "Blue"
    + "</label>"
    + "</div>"
    + "</div>";
    demo_settings.append(demo_settingshtml);
    demo.click(function() {
        if (!$(this).hasClass("open")) {
            $(this).css("right", "200px");
            demo_settings.css("right", "0");
            $(this).addClass("open");
        } else {
            $(this).css("right", "0");
            demo_settings.css("right", "-200px");
            $(this).removeClass("open");
        }
    });

    $("body").append(demo);
    $("body").append(demo_settings);
    
    
    var ajaxloadurl =  $(".ajax-load-url");
    
    ajaxloadurl.each(function(){
    	var this_ = $(this);
    	this_.load(this_.attr("ajax-url"));
    });
    
    
});

function change_layout() {
    $("body").toggleClass("fixed");
    fix_sidebar();
}
function change_skin(cls) {
	var weburl = $("script[src$='demo.js']").attr("src");
	weburl = weburl.substring(0,weburl.indexOf("js/AdminLTE/demo.js")) ; 
    $("body").removeClass("skin-blue skin-black");
    $("body").addClass(cls);
    var data = {theme:cls};
    $.get(weburl+"system/json/set.html",data);
}


(function($){
	$.fn.ctable = function(options){
		var weburl = $("script[src$='demo.js']").attr("src");
		weburl = weburl.substring(0,weburl.indexOf("js/AdminLTE/demo.js"));
		if(!options.fnDrawCallback){
			options.fnDrawCallback = function(){};
		}
				$(this).dataTable(
					{
					"oLanguage" : {"sUrl" : weburl+"system/json/cn.html"},
					"bProcessing" : true,
					"bServerSide" : true,
					"bPaginate" : true,
					"bAutoWidth" : false,
					"sAjaxSource" : options.sAjaxSource,
					"sServerMethod" : "POST",
					"aoColumns" :options.aoColumns ,
					"fnDrawCallback" : options.fnDrawCallback
				});
	};  
})(jQuery);

