
Date.prototype.format = function(format) {
       var date = {
              "M+": this.getMonth() + 1,
              "d+": this.getDate(),
              "h+": this.getHours(),
              "m+": this.getMinutes(),
              "s+": this.getSeconds(),
              "q+": Math.floor((this.getMonth() + 3) / 3),
              "S+": this.getMilliseconds()
       };
       if (/(y+)/i.test(format)) {
              format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
       }
       for (var k in date) {
              if (new RegExp("(" + k + ")").test(format)) {
                     format = format.replace(RegExp.$1, RegExp.$1.length == 1
                            ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
              }
       }
       return format;
};
var formatLong = function(str,format){
	return 	new Date(Number(str)).format(format);
};

var openModal = function(url,title){
		if(!title){
			title = "\u7a97\u53e3";
		}
		var html = "<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">" +
				"<div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\">" +
 	 		"<button type=\"button\" class=\"close\" data-dismiss=\"modal\"><span aria-hidden=\"true\">&times;</span><span class=\"sr-only\">Close</span></button>" +
 	 		"<h4 class=\"modal-title\" id=\"myModalLabel\">"+title+"</h4></div><div class=\"modal-body\"></div><div class=\"modal-footer\">" +
 	 		"<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>" +
 	 		"<button type=\"button\" class=\"btn btn-primary\">Save changes</button></div></div></div></div>";
	 	var dialog = $(html);
			dialog.on('hidden.bs.modal', function (e) {
				dialog.remove();
			});
			dialog.on('shown.bs.modal', function (e) {
				dialog.find(".modal-body").load(url);
		   });
           dialog.modal('show');
};

