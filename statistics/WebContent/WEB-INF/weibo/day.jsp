<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<html>
<body>
	<section class="content">
				<div class="row">
                        <div class="col-xs-12">
							 <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                       <li>
                                       	<a href="#tab_weibo" data-toggle="tab">微博排行</a>
                                       	</li>
                                </ul>
                                <div class="tab-content">
                                	 <div class="tab-pane  active " id="tab_0">
	                                    	<div class="box-body ajax-load-url" ajax-url="${WebUrl}weibo/table/${time}/type_${styleId}.html"></div>
	                                    </div><!-- /.tab-pane -->
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->
                </div><!-- /.row -->
	</section>
</body>
</html>
