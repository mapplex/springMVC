<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>序号</th>
				<th>标题</th>
				<th>点击量</th>
				<th>修改时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${slist}" var="sina" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${sina.id}</td>
						<td>${sina.title}</td>
						<td>${sina.weiboCount}</td>
						<td>${sina.startTime}</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
