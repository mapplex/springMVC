<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<html>
  <head>
  </head>
  <body>
<section class="content">
		<div class="row">
			<section class="col-md-6 connectedSortable">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">每日热门数据</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="userlist_table" class="table table-bordered">
							<thead>
								<tr>
									<th>名称</th>
									<th>时间</th>
									<th>操作</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<!-- /.box -->
			</section>
			<section class="col-md-6 connectedSortable">
			</section>
		</div>
	</section>
		<script type="text/javascript">
		$(function(){
		<%--
						$("#userlist_table").dataTable(
							{
								"oLanguage" : {
									"sUrl" : "${WebUrl}js/cn.txt"
								},
								"bProcessing" : true,
								"bServerSide" : true,
								"bPaginate" : true,
								"bAutoWidth" : false,
								"sAjaxSource" : "${WebUrl}hot/json/hotmonth.html",
								"sServerMethod" : "POST",
								"aoColumns" : [{"sName":"name","mData" : "name"},{"sName":"registertime","mData" : "registertime"},{"mData" : null,"sWidth" : "60","sDefaultContent" : '<a href="javascript:void(0)" class="openData">查看详情</a>'} ],
								 "fnDrawCallback" : function() {
								}
							});
							
					--%>		
		     $("#userlist_table").ctable({
				sAjaxSource : "${WebUrl}user/list.json",
				aoColumns : [{"sName":"name","mData" : "name"},{"sName":"registertime","mData" : "registertime","sClass" : "db_time"},{"mData" : null,"sWidth" : "60","sDefaultContent" : '<a href="javascript:void(0)" class="openData">查看详情</a>'} ],
				fnDrawCallback:function(){
							$("#userlist_table .db_time").each(function(index, ele) {
									if (index > 0) {
												$(this).text(formatLong($(this).text(),"yyyy-MM-dd hh:mm:ss"));
									}
						});
				}
			});	
		
		});
		</script>
  </body>
