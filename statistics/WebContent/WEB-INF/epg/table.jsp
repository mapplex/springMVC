<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<table class="table table-bordered" style="font-size: 14px;">
		<thead>
			<tr>
				<th>#</th>
				<th>标题</th>
				<th>演员</th>
				<th>导演</th>
				<th>地区</th>
				<th>类型</th>
				<th>年份</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${rlist}" var="reVideo" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${reVideo.title}</td>
						<td>${reVideo.actor}</td>
						<td>${reVideo.director}</td>
						<td>${reVideo.region}</td>
						<td>${reVideo.category}</td>
						<td>${reVideo.year}</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
