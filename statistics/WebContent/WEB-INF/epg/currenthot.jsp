<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <style type="text/css">
  .box {
float: left;
min-width: 30px;
min-height: 30px;
margin-top: 10px;
width: 308px;
height: 225px;
}
.box .pic{
  float:left;
  width: 150px;
  }
 .box .contentq{
   float:left;
   width: 150px;
   margin-left: 5px;
 }
  .contentq table{
  display: block;
  width: 150px;
  height: 150px;
  overflow: hidden;
  }
 .contentq table tr{
display: block;
float: left;
font-size: 12px;
  width: 150px;
 }
  .contentq table tr td{
  display: block;
  
  }
</style>
  </head>
  <body>
  <section class="content">
  	<div class="row">
  		<c:forEach items="${hlist}" var="h">
	  			 <div class="box">
	  			 	<div class="pic">
					  <img class="poster_img" height="200" type="" src="${h.imageLink}"   style="cursor: pointer;height:200px;width: 150px;float:left"> 
					  <span style="height:20px;width: 150px;float:left;display:block;text-align: center;margin-top: 5px">${h.title}</span>
	  			 	</div>
	  			 	<div class="contentq">
	  			 		<table>
	  			 			<tr>
	  			 				<td>主持/导演</td><td>${h.actor}</td>
	  			 			</tr>
	  			 			<tr>
	  			 				<td>嘉宾/演员</td><td>${h.director}</td>
	  			 			</tr>
	  			 		</table>
	  			 		<c:forEach items="${h.epgs}" var="epp" varStatus="status">
	  			 		
	  			 			<c:if test="${status.count==1}">
		  			 		<div class="dt">
			  			 		<div class="name">${epp.chaneseName}</div>
			  			 		<div class="progress xs progress-striped active" style="width: 100px;float:left">
				  			 		<div class="jindu progress-bar progress-bar-success" style="width: ${epp.percentage}%"></div>
			  			 		</div>
			  			 		<div style=" float: left;  width: 45px; font-size: 12px; margin-top: -5px; margin-left: 5px;">${epp.intervalTime}分钟</div>
			  			 		
			  			 		<div style=" float: left;  width:140px; font-size: 12px;margin-top: -16px;"><span>${epp.startTime}</span><span style="margin-left: 45px;">${epp.endTime}</span></div>
		  			 		</div>
		  			 		</c:if>
	  			 		</c:forEach>
	  			 	</div>
				</div>
  		</c:forEach>
  	</div>
  </section>
  </body>
</html>
