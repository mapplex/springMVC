<%@ page language="java" import="java.util.*,com.cooge.statistics.project.model.*,com.cooge.statistics.project.util.*" 

pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport"	content="width=device-width initial-scale=1.0 maximum-scale=1.0 user-scalable=yes" />
<title>jQuery.mmenu - Demo</title>
<link type="text/css" rel="stylesheet" href="${WebUrl}css/layout.css" />
<link type="text/css" rel="stylesheet"	href="${WebUrl}css/jquery.mmenu.widescreen.css"	media="all and (min-width: 1430px)" />
<link type="text/css" rel="stylesheet"	href="${WebUrl}css/jquery.mmenu.all.css" />
<link href="${WebUrl}css/table.css" rel="stylesheet"type="text/css" />
<script type="text/javascript" src="${WebUrl}js/jquery-2.1.1.js"></script>
<script type="text/javascript" src="${WebUrl}/js/jquery.mmenu.min.all.js"></script>
<script type="text/javascript">
	$(function() {
		$('nav#menu').mmenu({
			classes : 'mm-white mm-slide',
			counters : true,
			header : {
				add : true,
				update : true,
				title : '电视频道'
			},
			footer : {
				add : true,
				content : "(c) 2014"
			}
		});
		$("#hamburger").click();
		
		$(".jiemu-dan").each(function(){
			$(this).click(function(){
					$("#page .content").load("${WebUrl}epg/table/video.html?type="+$(this).attr

("videoType")+"&videoid="+$(this).attr("videoId"));
			});
		
		});
		
		
	});
</script>
</head>
<body>
	<div id="page">
		<a id="hamburger" class="FixedTop" href="#menu"><span></span></a>
		<div class="content" style="height: 100%;overflow-y: scroll;">
		
		</div>
	</div>
	<nav id="menu">
		<ul>
			<% 
			List<String> sslist = new ArrayList<String>();
			
			
				String time1 =TimeUtil.getTime(-3);
				String time2 =TimeUtil.getTime(-2);
				String time3 =TimeUtil.getTime(-1);
				String time4 =TimeUtil.getTime(0);
				String time5 =TimeUtil.getTime(1);
				String time6 =TimeUtil.getTime(2);
				String time7 =TimeUtil.getTime(3);
				sslist.add(time1);
				sslist.add(time2);
				sslist.add(time3);
				sslist.add(time4);
				sslist.add(time5);
				sslist.add(time6);
				sslist.add(time7);
			
			List<Epg> elist =  (List<Epg>)request.getAttribute("epglist");
				for(Epg e:elist){
				%>
				
				<li ><span><i class="fa fa-file-text-o"></i> &nbsp;<%=e.getName()%></span>
					<ul>
					
					<%    for(String eps:sslist){
					
					%>
					
					
					
						<li><span><%=eps%>&nbsp;&nbsp;节目单</span>
							<ul>
							<%
							
									List<Jiemu> jlist = Epg.getJiemus(e.getStyle(),eps);
									for(Jiemu jm:jlist){
									
										if(jm.getClassId().equals("U")||jm.getTitle().contains("长女的美丽人生")||jm.getTitle().contains("老农民")){
										
										
										%>
										<li><a  href="javascript:void(0)"> &nbsp;<%=jm.getTitle() %>  <span style="float: right;"><%=jm.getStartTime() %></span></a> </li>
										<%
										
										}else{
										if(jm.getTitle().contains("摩尔庄园")&&jm.getVideoId().equals("12211")){
											jm.setVideoId("44277");
										}
										
										%>
										<li><a class="jiemu-dan" videoId="<%=jm.getVideoId()%>" videoType="<%=jm.getClassId()%>" href="javascript:void(0)" style="color: green;"> &nbsp;<%=jm.getTitle() %>  <span style="float: right;color: rgba(0, 0, 0, 0.6);"><%=jm.getStartTime() %></span></a> </li>
										<%
										}
											
									}
							
							 %>
							
							</ul>
						</li>
					
					<%
					} 
					%>
						
					</ul>
				</li>
				
				<%
				
				}
			 %>
		</ul>
	</nav>
</body>
</html>