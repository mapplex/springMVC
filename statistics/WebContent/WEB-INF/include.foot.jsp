<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- jQuery 2.0.2 -->
<script src="${WebUrl}js/jquery-2.1.1.js" type="text/javascript"></script>
<!-- jQuery UI 1.10.3 -->
<script src="${WebUrl}js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="${WebUrl}js/bootstrap.min.js" type="text/javascript"></script>
<!-- Morris.js charts -->
<script	src="${WebUrl}js/raphael-min.js"></script>
<script src="${WebUrl}js/plugins/morris/morris.min.js"	type="text/javascript"></script>
<!-- Sparkline -->
<script src="${WebUrl}js/plugins/sparkline/jquery.sparkline.min.js"	type="text/javascript"></script>
<!-- jvectormap -->
<script	src="${WebUrl}js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"	type="text/javascript"></script>
<script	src="${WebUrl}js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"	type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="${WebUrl}js/plugins/jqueryKnob/jquery.knob.js"	type="text/javascript"></script>
<!-- daterangepicker -->
<script src="${WebUrl}js/plugins/daterangepicker/daterangepicker.js"	type="text/javascript"></script>
<!-- datepicker -->
<script src="${WebUrl}js/plugins/datepicker/bootstrap-datepicker.js"	type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script	src="${WebUrl}js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"	type="text/javascript"></script>
<!-- iCheck -->
<script src="${WebUrl}js/plugins/iCheck/icheck.min.js"	type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="${WebUrl}js/AdminLTE/app.js" type="text/javascript"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--  
<script src="${WebUrl}js/AdminLTE/dashboard.js" type="text/javascript"></script>
-->
<!-- AdminLTE for demo purposes -->
<script src="${WebUrl}js/AdminLTE/demo.js" type="text/javascript"></script>
<script src="${WebUrl}js/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
<!-- FLOT RESIZE PLUGIN - allows the chart to redraw when the window is resized -->
<script src="${WebUrl}js/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
<!-- FLOT PIE PLUGIN - also used to draw donut charts -->
 <script src="${WebUrl}js/plugins/flot/jquery.flot.pie.min.js" type="text/javascript"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="${WebUrl}js/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
<script src="${WebUrl}js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="${WebUrl}js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="${WebUrl}js/util.js" type="text/javascript"></script>


