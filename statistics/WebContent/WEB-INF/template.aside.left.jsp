<%@ page language="java" import="java.util.*,com.cooge.statistics.common.util.*,com.cooge.statistics.common.model.*" pageEncoding="UTF-8"%>
<aside class="left-side sidebar-offcanvas">
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="${WebUrl}img/avatar3.png" class="img-circle" alt="User Image" />
			</div>
			<div class="pull-left info">
				<p>Hello, <%=((UserInfo)request.getSession().getAttribute(URLUtil.UID)).getName() %></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control"
					placeholder="Search..." /> <span class="input-group-btn">
					<button type='submit' name='seach' id='search-btn'
						class="btn btn-flat">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
		<ul class="sidebar-menu">
		<%=request.getAttribute(URLUtil.LEFTMENU).toString()%>
		</ul>
	</section>
	<!-- /.sidebar -->
</aside>