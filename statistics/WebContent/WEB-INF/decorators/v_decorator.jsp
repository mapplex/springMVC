<%@ page pageEncoding="UTF-8" import="com.cooge.statistics.common.model.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>数据统计中心</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<%@ include file="../include.head.jsp"%>
<%@ include file="../include.foot.jsp"%>
<sitemesh:write property='head'/>
</head>
<body class="<%=((SystemSet)request.getSession().getAttribute(URLUtil.SYSTEMSET)).getTheme() %>">
<%@ include file="../template.head.jsp"%>
<div class="wrapper row-offcanvas row-offcanvas-left">
<%@ include file="../template.aside.left.jsp"%>
<aside class="right-side">
	<section class="content-header">
		<h1>
			<%=request.getAttribute(URLUtil.REQUESTTITLE) %>
		</h1>
		<ol class="breadcrumb">
			<%=request.getAttribute(URLUtil.REQUESNAV) %>
		</ol>
	</section>
<sitemesh:write property='body' />
</aside>
</div>
</body>
</html>
