<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>游戏</title>
<link rel="stylesheet" href="${WebUrl}/css/bootstrap.min.css">
<link rel="stylesheet" href="${WebUrl}/css/cooge.css">
<script src="${WebUrl}/js/jquery.min.js"></script>
<script src="${WebUrl}/js/bootstrap.min.js"></script>
<script src="${WebUrl}/js/jquery.devrama.lazyload-0.9.3.js"></script>
<sitemesh:write property='head'/>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<form class="navbar-form navbar-left" role="search">
				<div class="form-group">
					<input type="text" name="key" class="form-control" placeholder="关键词">
				</div>
				<button type="submit" class="btn btn-default">搜索</button>
			</form>
		</div>
	</nav>
	<div id="view">
	<sitemesh:write property='body' />
	</div>
	<div id="re_keyword">

	<div class="panel panel-default">
  <div class="panel-body">
    Basic panel example
  </div>
</div>
</div>
</body>
<script type="text/javascript">
$(function(){
	$.DrLazyload();
	
	$("form").submit(function(){
	     var s = $(this).find("[name='key']").val();
	     window.location.href = "${WebUrl}/search_video/s_"+s+".html";
	    return false;
	
	});
	
});
</script>
</html>
