<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% 
  Date date = new Date();
  String romid =  String.valueOf(date.getTime()).substring(6);
%>
<div id="bar-chart<%=romid%>" style="height: 300px;"></div>
<script type="text/javascript">
$(function(){
Morris.Bar({
  element: 'bar-chart<%=romid%>',
  resize: true,
  data: ${data},
  xkey: 'x',
  ykeys: ${labes},
  labels: ${labes}
}).on('click', function(i, row){
  console.log(i, row);
});

});
</script>				
				
