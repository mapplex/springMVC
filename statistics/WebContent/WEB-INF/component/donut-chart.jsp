<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% 
  Date date = new Date();
  String romid =  String.valueOf(date.getTime()).substring(6);
%>
<div class="chart" id="sales-chart<%=romid%>" style="height: 300px; position: relative;"></div>
<script type="text/javascript">
$(function(){
new Morris.Donut({
                    element: 'sales-chart<%=romid%>',
                    resize: true,
                    colors: ${colors},
                    data: ${chartValues},
                    hideHover: 'auto'
        });
});
</script>