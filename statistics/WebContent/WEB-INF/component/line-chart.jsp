<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<% 
  Date date = new Date();
  String romid =  String.valueOf(date.getTime()).substring(6);
%>
<div class="chart" id="line-chart<%=romid%>" style="height: 300px;"></div>
<script type="text/javascript">
$(function(){
new Morris.Line({
element: 'line-chart<%=romid%>',
resize: true,
data: ${charlinedata},
		xkey: 'name',
		ykeys: ['number'],
		labels: ['数值'],
		lineColors: ['#3c8dbc'],
		hideHover: 'auto',
		xLabels:'day'
});
});
</script>
