<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>登陆</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="${WebUrl}css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="${WebUrl}css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="${WebUrl}css/AdminLTE.css" rel="stylesheet" type="text/css" />
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">登陆</div>
            <form action="${WebUrl}login.html" method="post">
                <div class="body bg-gray">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="用户名"/>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="密码"/>
                    </div>          
                    <div class="form-group">
                        <input type="checkbox" name="remember_me"/> 记住密码
                    </div>
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">登陆</button>  
                    <p><a href="#">忘记密码</a></p>
                    <a href="register.html" class="text-center">注册一个账号</a>
                </div>
                <input type="hidden" name="pUrl" value="${param.pUrl}">
            </form>
        </div>
        <script src="${WebUrl}js/jquery-2.1.1.js" type="text/javascript"></script>
        <script src="${WebUrl}js/bootstrap.min.js" type="text/javascript"></script>        
    </body>
</html>