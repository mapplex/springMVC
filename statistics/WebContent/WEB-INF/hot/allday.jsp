<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<html>
<body>
	<section class="content">
				<div class="row">
                        <div class="col-xs-12">
							 <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                     <li   class="active" >
                                       	<a href="#tab_0" data-toggle="tab">总榜</a>
                                       	</li>
                                	<c:forEach items="${rkslist}" var="rks" varStatus="status">
                                       <li>
                                       	<a href="#tab_${status.count}" data-toggle="tab">${rks.name}</a>
                                       	</li>
                                    </c:forEach>
                                </ul>
                                <div class="tab-content">
                                	 <div class="tab-pane  active " id="tab_0">
	                                    	<div class="box-body ajax-load-url" ajax-url="${WebUrl}hot/table/day/type_${styleId}.html"></div>
	                                    </div><!-- /.tab-pane -->
                                	<c:forEach items="${rkslist}" var="rks" varStatus="status">
	                                    <div class="tab-pane   " id="tab_${status.count}">
	                                    	<div class="box-body ajax-load-url" ajax-url="${WebUrl}hot/table/day/type_${styleId}/webSite_${rks.id}.html"></div>
	                                    </div><!-- /.tab-pane -->
                                    </c:forEach>
                                </div><!-- /.tab-content -->
                            </div><!-- nav-tabs-custom -->
                        </div><!-- /.col -->
                </div><!-- /.row -->
	</section>
</body>
</html>