<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
	<table class="table table-bordered" id="alltable">
		<thead>
			<tr>
				<th>#</th>
				<th>标题</th>
				<th>访问数量</th>
				<th>添加时间</th>
				<th>查看分布</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${hotlist}" var="hot" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${hot.title}</td>
						<td><span class="badge bg-red">${hot.count}</span></td>
						<td>${hot.addTime}</td>
						<th><a href="javascript:void(0)" class="queryChat" title="${hot.title}" videoId="${hot.id}">查看</a></th>
					</tr>
			</c:forEach>
		</tbody>
	</table>
<script type="text/javascript">
$(function(){
	var query = $("#alltable tbody th .queryChat");
	query.each(function(index,ele){
	 $(this).click(function(){
		openModal("${WebUrl}hot/chart/${time}/type_${styleId}/videoId_"+$(this).attr("videoId")+".html",$(this).attr("title"));
	 });
	});
});
</script>