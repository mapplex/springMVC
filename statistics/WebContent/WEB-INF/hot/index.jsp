<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
<html>
<body>
	<section class="content">
		<div class="row">
			<section class="col-md-6 connectedSortable">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">每日热门数据</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="hotdayTop_table" class="table table-bordered">
							<thead>
								<tr>
									<th>名称</th>
									<th>数量</th>
									<th>时间</th>
									<th>操作</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<!-- /.box -->
			</section>
			<section class="col-md-6 connectedSortable">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">每月热门数据</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="hotmonthTop_table" class="table table-bordered">
							<thead>
								<tr>
									<th>名称</th>
									<th>数量</th>
									<th>时间</th>
									<th>操作</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<!-- /.box -->
			</section>
		</div>
	</section>
	<script type="text/javascript">
		//"finalRank","title","updateTime"
		$(function() {
		
			$(".connectedSortable").sortable({
				placeholder : "sort-highlight",
				connectWith : ".connectedSortable",
				handle : ".box-header, .nav-tabs",
				forcePlaceholderSize : true,
				zIndex : 999999
			}).disableSelection();
			
			
			$("#hotdayTop_table").ctable({
					aoColumns : [{"sName":"title","mData" : "title"},{"sName":"finalRank","mData" : "finalRank"},{"sName":"updateTime","mData" : "updateTime","sClass" : "db_time"},
								 {"mData" : null,"sWidth" : "60","sDefaultContent" : '<a href="javascript:void(0)" class="openData">查看详情</a>'} ],
				fnDrawCallback : function() {
								$("#hotdayTop_table .db_time").each(function(index, ele) {
									if (index > 0) {$(this).text(formatLong($(this).text(),"yyyy-MM-dd h:m:s"));}
								});

								$("#hotdayTop_table .openData").click(function() {
											openModal("${WebUrl}hot/chart/line.html","图表");
								});
						}
			
			});
			<%--
			
			
			
			$("#hotdayTop_table")
					.dataTable(
							{
								"oLanguage" : {
									"sUrl" : "${WebUrl}js/cn.txt"
								},
								"bProcessing" : true,
								"bServerSide" : true,
								"bPaginate" : true,
								"bAutoWidth" : false,
								"sAjaxSource" : "${WebUrl}hot/json/hotday.html",
								"sServerMethod" : "POST",
								"aoColumns" : [{"sName":"title","mData" : "title"},{"sName":"finalRank","mData" : "finalRank"},{
											"sName":"updateTime","mData" : "updateTime",
											"sClass" : "db_time"
										},
										{
											"mData" : null,
											"sWidth" : "60",
											"sDefaultContent" : '<a href="javascript:void(0)" class="openData">查看详情</a>'
										} ],
								"fnDrawCallback" : function() {
									$("#hotdayTop_table .db_time").each(function(index, ele) {
														if (index > 0) {
															$(this).text(formatLong($(this).text(),"yyyy-MM-dd h:m:s"));
														}
													});

									$("#hotdayTop_table .openData").click(function() {
												openModal("${WebUrl}hot/chart/line.html","图表");
									});
								}
							});
--%>
			$("#hotmonthTop_table")
					.dataTable(
							{
								"oLanguage" : {
									"sUrl" : "${WebUrl}js/cn.txt"
								},
								"bProcessing" : true,
								"bServerSide" : true,
								"bPaginate" : true,
								"bAutoWidth" : false,
								"sAjaxSource" : "${WebUrl}hot/json/hotmonth.html",
								"sServerMethod" : "POST",
								"aoColumns" : [{"sName":"title","mData" : "title"},{"sName":"finalRank","mData" : "finalRank"},{"sName":"updateTime","mData" : "updateTime","sClass" : "db_time"},{"mData" : null,"sWidth" : "60","sDefaultContent" : '<a href="javascript:void(0)" class="openData">查看详情</a>'} ],
								 "fnDrawCallback" : function() {
									$("#hotmonthTop_table .db_time").each(function(index, ele) {
														if (index > 0) {
															$(this).text(formatLong($(this).text(),"yyyy-MM-dd hh:mm:ss"));
														}
										});
									$("#hotmonthTop_table .openData").click(function() {
												openModal("${WebUrl}hot/chart/donutline.html");
									});
								}
							});

		});
	</script>
</body>
</html>