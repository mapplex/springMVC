<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="../include.inc.jsp"%>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>#</th>
				<th>标题</th>
				<th>访问数量</th>
				<th>添加时间</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${hotlist}" var="hot" varStatus="status">
					<tr>
						<td>${status.count}</td>
						<td>${hot.title}</td>
						<td><span class="badge bg-red">${hot.count}</span></td>
						<td>${hot.addTime}</td>
					</tr>
			</c:forEach>
		</tbody>
	</table>
