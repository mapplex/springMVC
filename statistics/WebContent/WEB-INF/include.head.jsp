<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- bootstrap 3.0.2 -->
<link href="${WebUrl}css/bootstrap.min.css" rel="stylesheet"type="text/css" />
<!-- font Awesome -->
<link href="${WebUrl}css/font-awesome.min.css" rel="stylesheet"type="text/css" />
<!-- Ionicons -->
<link href="${WebUrl}css/ionicons.min.css" rel="stylesheet"type="text/css" />
<!-- Morris chart -->
<link href="${WebUrl}css/morris/morris.css" rel="stylesheet"type="text/css" />
<!-- jvectormap -->
<link href="${WebUrl}css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
<!-- Date Picker -->
<link href="${WebUrl}css/datepicker/datepicker3.css" rel="stylesheet"type="text/css" />
<!-- Daterange picker -->
<link href="${WebUrl}css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
<!-- bootstrap wysihtml5 - text editor -->
<link href="${WebUrl}css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="${WebUrl}css/AdminLTE.css" rel="stylesheet" type="text/css" />
<link href="${WebUrl}css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<style type="text/css">
.dataTables_processing{
text-align: center;
height: 0px;
}
</style>


